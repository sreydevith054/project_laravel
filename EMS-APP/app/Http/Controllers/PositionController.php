<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search =  $request->input('search');
        if($search!=""){
            $position = Position::where(function ($query) use ($search){
                $query->where('position', 'like', '%'.$search.'%')
                    ->orWhere('description', 'like', '%'.$search.'%');
            })
            ->paginate(2);
            $position->appends(['search' => $search]);
        }
        else{
            $position =Position::paginate(4);
        }
        return view('livewire.admin.position',['position'=>$position,'search'=>$search]);
     }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department=Department::all();
        return view('livewire.position.create',['department'=>$department]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'department_id' => 'required',
            'position'=> 'required',
            'description' => 'required',
            // 'phone_num' => 'required',
            // 'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        // $newImagename=time(). '-'.$request->department. '.'.$request->image->extension();
        // $request->image->move(public_path('image'),$newImagename);
        //    department::create($request->all());
        Position::create([
            'department_id' =>$request->input('department_id'),
            'position' => $request->input('position'),
            'description' => $request->input('description')
            // 'image_path'=>$newImagename
        ]);
            return redirect()->route('position.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        return view('livewire.position.show',['position'=>$position]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function edit(Position $position)
    {
        $department=Department::all();
        return view('livewire.position.edit',['position'=>$position,'department'=>$department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Position $position)
    {
        $request->validate([
            'department_id' => 'required',
            'position'=> 'required',
            'description' => 'required',
            // 'phone_num' => 'required',
            // 'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        // $newImagename=time(). '-'.$request->department. '.'.$request->image->extension();
        // $request->image->move(public_path('image'),$newImagename);
        //    department::create($request->all());
        $position->update([
            'department_id' =>$request->input('department_id'),
            'position' => $request->input('position'),
            'description' => $request->input('description')
            // 'image_path'=>$newImagename
        ]);
            return redirect()->route('position.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function destroy(Position $position)
    {
        $position->delete();
        return redirect()->route('position.index');

    }
}
