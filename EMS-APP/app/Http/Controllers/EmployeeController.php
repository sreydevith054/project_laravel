<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use App\Models\Department;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search =  $request->input('search');
        if($search!=""){
            $employee= Employee::where(function ($query) use ($search){
                $query->where('name','like', '%'.$search.'%')
                    ->orWhere('email', 'like', '%'.$search.'%')
                    ->orWhere('cell_phone_number', 'like', '%'.$search.'%')
                    ->orWhere('position_id', 'like', '%'.$search.'%')
                    ;
            }
            )
            ->paginate(4);

            $employee->appends(['search' => $search]);
        }

        else{
            $employee = Employee::paginate(4);
        }
        return view('livewire.admin.employee',['employee'=>$employee,'search'=>$search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position=Position::all();
        return view('livewire.Employee.create',['position'=>$position]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'user_id'=>'required',
            'position_id'=>'required',
            'name'=>'required',
            'gender'=>'required',
            'address'=>'required',
            'phone_number'=>'required',
            'cell_phone_number'=>'required',
            'email'=>'required',
            'dob'=>'required',
            'learning_institute'=>'required',
            'marital_status'=>'required',
            'description'=>'required',
            // 'Spouses_name',
            // 'Spouses_contact',
            'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        $newImagename=time(). '-'.$request->name.'.'.$request->image->extension();
        $request->image->move(public_path('Employee'),$newImagename);
        //    department::create($request->all());
        Employee::create([
            'user_id'=>$request->input('user_id'),
            'position_id'=>$request->input('position_id'),
            'name'=>$request->input('name'),
            'gender'=>$request->input('gender'),
            'address'=>$request->input('address'),
            'phone_number'=>$request->input('phone_number'),
            'cell_phone_number'=>$request->input('cell_phone_number'),
            'email'=>$request->input('email'),
            'dob'=>$request->input('dob'),
            'learning_institute'=>$request->input('learning_institute'),
            'marital_status'=>$request->input('marital_status'),
            'description'=>$request->input('description'),
            'Spouses_name'=>$request->input('Spouses_name'),
            'Spouses_contact'=>$request->input('Spouses_contact'),
            'image_path'=>$newImagename
        ]);
            return redirect()->route('employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('livewire.Employee.show',['employee'=>$employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $position=Position::all();
        return view('livewire.employee.edit',['position'=>$position,'employee'=>$employee]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $request->validate([
            // 'user_id'=>'required',
            'position_id'=>'required',
            'name'=>'required',
            'gender'=>'required',
            'address'=>'required',
            'phone_number'=>'required',
            'cell_phone_number'=>'required',
            'email'=>'required',
            'dob'=>'required',
            'learning_institute'=>'required',
            'marital_status'=>'required',
            'description'=>'required',
            // 'Spouses_name',
            // 'Spouses_contact',
            // 'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        // $newImagename=time(). '-'.$request->name.'.'.$request->image->extension();
        // $request->image->move(public_path('Employee'),$newImagename);
        //    department::create($request->all());
        $input = $request->all();
        if ($image = $request->image) {
            // $destinationPath = 'image/';
            $newImagename = time(). '-'.$request->name.'.'.$request->image->extension();
            // $image->move($destinationPath, $profileImage);
            $input['image_path'] = $newImagename;
            $image->move(public_path('Employee'),$newImagename);
        }else{
            unset($input['image_path']);
        }
        // $employee->update([
        //     'user_id'=>$request->input('user_id'),
        //     'position_id'=>$request->input('position_id'),
        //     'name'=>$request->input('name'),
        //     'gender'=>$request->input('gender'),
        //     'address'=>$request->input('address'),
        //     'phone_number'=>$request->input('phone_number'),
        //     'cell_phone_number'=>$request->input('cell_phone_number'),
        //     'email'=>$request->input('email'),
        //     'dob'=>$request->input('dob'),
        //     'learning_institute'=>$request->input('learning_institute'),
        //     'marital_status'=>$request->input('marital_status'),
        //     'description'=>$request->input('description'),
        //     'Spouses_name'=>$request->input('Spouses_name'),
        //     'Spouses_contact'=>$request->input('Spouses_contact'),
        //     // 'image_path'=>$newImagename
        // ]);
        $employee->update($input);
                return redirect()->route('employee.index')
                        ->with('success','Employee updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return redirect()->route('employee.index');
    }
}
