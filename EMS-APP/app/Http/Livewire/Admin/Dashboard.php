<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Livewire\Component;
use App\Models\Employee;
use App\Models\Department;
use Livewire\WithPagination;

class Dashboard extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $users=User::count();
        $department=Department::count();
        $employee=Employee::count();
        // $panel=User::all();
        // dd($panel);
        return view('livewire.admin.dashboard',
        [
        'users'=>$users,
        'department'=>$department,
        'panel' => User::paginate(1),
        'employee'=>$employee
        ]
        )->layout('layouts.base');
    }
}
