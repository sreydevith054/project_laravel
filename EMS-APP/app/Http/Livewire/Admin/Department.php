<?php
// <?php

namespace App\Http\Livewire\Admin;
use App\Models\Department as ModelsDepartment;
use Livewire\Component;

class Department extends Component
{
//         public $department,$description,$phone_num,$image_path;
//         public $isModalOpen = 0;
    public $department;
    public function render()
    {
        $this->department=ModelsDepartment::all();
        return view('livewire.admin.department')->layout('layouts.base');
    }
    // public function create()
    // {
    //     $this->resetCreateForm();
    //     $this->openModalPopover();
    // }
    // public function openModalPopover()
    // {
    //     $this->isModalOpen = true;
    // }

    // public function closeModalPopover()
    // {
    //     $this->isModalOpen = false;
    // }
    // private function resetCreateForm(){
    //     $this-> department= '';
    //     $this->description = '';
    //     $this->phone_num= '';
    // }

    // public function store()
    // {
    //     $this->validate([
    //         'department' => 'required',
    //         'description' => 'required',
    //         'phone_num' => 'required',
    //     ]);

    //   ModelsDepartment::updateOrCreate(['id' => $this->id], [
    //         'department' => $this->department,
    //         'description' => $this->description,
    //         'phone_num' => $this->phone_num,
    //     ]);

    //     session()->flash('message', $this->id ? 'department updated.' : 'department created.');
    //     $this->closeModalPopover();
    //     $this->resetCreateForm();
    // }
    // public function edit($id)
    // {
    //     $department = ModelsDepartment::findOrFail($id);
    //     $this->id = $id;
    //     $this->department =$department->department;
    //     $this->description=$department->description;
    //     $this->phone_num =$department->phone_num;
    //     $this->updateMode = true;
    //     $this->openModalPopover();
    // }

    // public function delete($id)
    // {
    //     ModelsDepartment::find($id)->delete();
    //     session()->flash('message', 'department deleted.');
    // }}
    }
