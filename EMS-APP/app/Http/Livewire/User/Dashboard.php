<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\ComponentConcerns\RendersLivewireComponents;

class Dashboard extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $users=User::count();
        // $panel=User::all();
        // dd($panel);
        return view('livewire.user.dashboard',['users'=>$users,
        'panel' => User::paginate(1)])->layout('layouts.base');
    }
}
