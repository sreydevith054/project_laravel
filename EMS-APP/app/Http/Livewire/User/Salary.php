<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class Salary extends Component
{
    public function render()
    {
        return view('livewire.user.salary');
    }
}
