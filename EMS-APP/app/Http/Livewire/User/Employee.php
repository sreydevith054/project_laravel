<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class Employee extends Component
{
    public function render()
    {
        return view('livewire.user.employee');
    }
}
