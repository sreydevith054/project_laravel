<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use App\Models\Employee;
use App\Models\Department;
use Illuminate\Http\Request;
use Livewire\WithPagination;
use Illuminate\Support\Facades\App;

class Dasboard extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $user=user::all();
        $users=User::count();
        $department=Department::count();
        $employee=Employee::count();
        return view('livewire.dasboard',[
            'user'=>$user,
            'users'=>$users,
            'department'=>$department,
            'panel' => User::paginate(1),
            'employee'=>$employee])->layout('layouts.guest');
    }
    public function change(Request $request)

    {
        App::setLocale($request->lang);

        session()->put('locale', $request->lang);

        return redirect()->back();

    }
}
