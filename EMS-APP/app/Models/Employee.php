<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'position_id',
        'name',
        'gender',
        'address',
        'phone_number',
        'cell_phone_number',
        'email',
        'dob',
        'learning_institute',
        'marital_status',
        'description',
        'Spouses_name',
        'Spouses_contact',
        'image_path'
    ];
    public function position()
    {
        return $this->belongsTo(Position::class);
    }

}
