<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Position extends Model
{
    use HasFactory;
    protected $fillable=[
        'position',
        'department_id',
        'description'
    ];
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function employee()
    {
        return $this->hasMany(Employee::class);
    }

}
