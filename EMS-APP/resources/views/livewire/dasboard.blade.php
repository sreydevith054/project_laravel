<div>
    @section('title','home')
    <div class="main-content">
        <section class="section">
          {{-- <h1 class="section-header">
            <div>Dashboard</div>
          </h1> --}}
          <div class="row">
            <div class="col-lg-3 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-primary">
                  <i class="ion ion-person"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>{{ __('massages.TOTAL USERS') }}</h4>
                  </div>
                  <div class="card-body">
                    {{$users}}
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-danger">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>{{ __('massages.Employee') }}</h4>
                  </div>
                  <div class="card-body">
                    {{ $employee}}
                </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-success">
                    <i class="far fa-building"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>{{ __('massages.DEPARTMENT') }}</h4>
                  </div>
                  <div class="card-body">
                   {{ $department }}
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-lg-9 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="float-right">
                    <nav class="d-inline-block">
                        {{ $panel->links() }}

                      </nav>
                     </div>
                  <h4>Users Pannel</h4>
                </div>
                <div class="card-body text-center">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Kh_Name</th>
                          <th>En_Name</th>
                          <th>Role</th>
                          <th>Contact</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($user as $users1)
                          <tr>
                            <td>
                              {{ $users1['id'] }}
                            </td>
                            <td>
                                <a href="{{ route('login') }}"><img src="{{ asset('storage/'.$users1->profile_photo_path) }}" alt="{{ $users1->name }}" width="50" class="rounded-circle img-thumbnail mr-1">{{ $users1['khName'] }}</a>
                            </td>

                            <td>
                                {{ $users1['name'] }}
                            </td>
                            <td>
                              {{ $users1['user_type'] }}
                            </td>
                            <td>
                                {{ $users1['phone'] }}
                            </td>
                          </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

</div>
