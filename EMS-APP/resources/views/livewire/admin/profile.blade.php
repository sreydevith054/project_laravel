{{-- <div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                @livewire('profile.update-profile-information-form')

                <x-jet-section-border />
            @endif

            @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                <div class="mt-10 sm:mt-0">
                    @livewire('profile.update-password-form')
                </div>

                <x-jet-section-border />
            @endif

            @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                <div class="mt-10 sm:mt-0">
                    @livewire('profile.two-factor-authentication-form')
                </div>

                <x-jet-section-border />
            @endif

            <div class="mt-10 sm:mt-0">
                @livewire('profile.logout-other-browser-sessions-form')
            </div>

            @if (Laravel\Jetstream\Jetstream::hasAccountDeletionFeatures())
                <x-jet-section-border />

                <div class="mt-10 sm:mt-0">
                    @livewire('profile.delete-user-form')
                </div>
            @endif
        </div>
    </div>
</div> --}}
<div>

        @section('title','Admin Profile')
        @push('css')
        <style>
        input[type=text]:disabled{
            background: white;
          }
          textarea[type=text]:disabled{
            background: white;
          }
        </style>
        @endpush
            @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                <div class="row">
                    <div class="col-md-6">
                        <section class="section">
                            <h1 class="section-header">
                                <div>Profile Information</div>
                            </h1>

                <div class="row">
                    <div class="col-lg-5 margin-tb">
                    </div>
                </div>


                    @livewire('profile.update-profile-information-form')
                </div>
            @endif

                {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        <input  type="text" value="" name="name" class="form-control" placeholder="Name">
                        </select>
                    </div>
                </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                <div class="col-xs-8 col-sm-8 col-md-8">
                    <div class="form-group">
                        <strong>UserName:</strong>
                        <input  type="text" value="" name="name" class="form-control" placeholder="Name">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <strong>Upload Profile:</strong>
                        <input  type="file" value="" name="name" class="form-control" placeholder="Name">
                    </div>
                </div>
                </div>
            </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input placeholder="email" type="email" value="" name="dob" class="form-control">
                    </div>
                </div> --}}


             <div class="col-md-6">

            <div class="user-image mt-3 text-center">

                    <div style="width: 300px; height: 300px; overflow: hidden; background: #cccccc; margin: 0 auto">
                        <img src="{{ asset('storage/'.Auth::user()->profile_photo_path) }}" class="figure-img img-thumbnail rounded"  id="imgPlaceholder" alt="">

                    </div>

              </div>
            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
            </div>
        </div>
    </div>
    </section>
    @push('script')
    <script>
        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
              $('#imgPlaceholder').attr('src', e.target.result);
            }

            // base64 string conversion
            reader.readAsDataURL(input.files[0]);
          }
        }

        $("#chooseFile").change(function () {
          readURL(this);
        });

    $("input").on("change", function() {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
            )
        }).trigger("change")
            </script>

    @endpush





</div>
