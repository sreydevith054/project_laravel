<x-base-layout>
    <div>
        @section('title','All Position')

        <section class="section">
            @if (session()->has('message'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                    role="alert">
                    <div class="flex">
                        <div>
                            <p class="text-sm">{{ session('message') }}</p>
                        </div>
                    </div>
                </div>
            @endif
                <h1 class="section-header">
                    <div>All Position</div>
            </h1>
            <div class="section-body">
              <div class="row mt-5">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">

                      <div class="float-right">
                      <form action="">
                        <div class="input-group">
                          <input type="text" id="myInput" name="search" value="{{ $search }}" class="form-control" placeholder="Search">
                          <div class="input-group-btn">
                            <button type="submit" id="myBtn" class="btn btn-secondary"><i class="ion ion-search"></i></button>
                          </div>
                        </div>
                      </form>
                        </div>
                      <h4>Position List</h4>
                      <a href="{{ route('position.create') }}" class="btn btn-sm btn-primary ion-plus-circled"></a>

                    </div>

                    <div class="card-body">

                      <div class="table-responsive">
                        <table class="table table-striped">
                          <tbody>
                              <tr>
                            <th>ID</th>
                            <th>Position</th>
                            <th>Department</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                          @foreach($position as $positions)
                            <td>{{ $positions['id'] }}</td>
                            <td>
                                {{ $positions['position'] }}
                            </td>
                            <td>
                               {{ $positions->department->department }}
                            <td>{{ $positions['description'] }}</td>
                            <td>
                                <form action="{{ route('position.destroy',$positions->id) }}" method="POST">
                                    <a href=" {{route('position.show',$positions['id'])}}" class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="view"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('position.edit',$positions['id']) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>

                              @csrf
                              @method('DELETE')
                    <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></button>
                            </form>

                            </td>
                          </tr>
                            @endforeach
                        </tbody>
                    </table>
                      </div>
                      <div class="d-flex justify-content-end">
                        {!! $position->links() !!}
                    </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
</x-base-layout>
