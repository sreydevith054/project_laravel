<x-base-layout>
    <div>
        @section('title','All Employee')

        <section class="section">
            @if (session()->has('message'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                    role="alert">
                    <div class="flex">
                        <div>
                            <p class="text-sm">{{ session('message') }}</p>
                        </div>
                    </div>
                </div>
            @endif
                <h1 class="section-header">
                    {{-- @if ($updateMode) --}}
                    <div>All Employee</div>
                    {{-- @else --}}
                    {{-- <div>Setting employee</div> --}}
                    {{-- @endif --}}
            </h1>
            {{-- @if($isModalOpen) --}}
            {{-- @endif --}}
            <div class="section-body">
              <div class="row mt-5">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">

                      <div class="float-right">
                          {{-- SEARCH FUNCTION --}}
                        <form action="">
                          <div class="input-group">
                            <input type="text" id="myInput" name="search" value="{{ $search }}" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                              <button type="submit" id="myBtn" class="btn btn-secondary"><i class="ion ion-search"></i></button>
                            </div>
                          </div>
                        </form>

                      </div>
                      <h4>Employee List</h4>
                      <a href="{{ route('employee.create') }}" class="btn btn-sm btn-primary ion-plus-circled"></a>
                    </div>

                    <div class="card-body">
                      <div class="table-responsive">
                        <div class="d-flex justify-content-end">
                        </div>
                        <table class="table table-striped">
                          <tbody>
                            <tr>
                            <th>ID</th>
                            <th>Profile</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Department</th>
                            {{-- <th>Description</th> --}}
                            {{-- <th>Status</th> --}}
                            <th>Contact</th>
                            <th>Action</th>

                          </tr>
                          @foreach($employee as $employees)
                          <tr>
                            <td>{{ $employees['id'] }}</td>
                            <td>
                                <div class="user-image mb-3 text-center">

                                    <div  style="width: 100px; height: 100px; overflow: hidden; background: #cccccc; margin: 0 auto;">
                                        <img  src="{{ URL::to('Employee/'.$employees['image_path']) }}" class="figure-img img-thumbnail"  id="imgPlaceholder" alt="">
                                    </div>

                              </div>

                                {{-- <img alt="image" src="{{ asset('Employee/'.$employees['image_path']) }}" class="rounded" width="75" data-toggle="title" title="Wildan Ahdian"> --}}
                              </td>

                            <td>
                                {{ $employees['name'] }}
                                {{-- {{ $departments['department'] }} --}}
                            </td>
                            <td>
                                {{ $employees['position']->position}}
                            </td>
                            {{-- <td>
                                <div class="badge badge-success">Allow</div>
                            </td> --}}
                            <td>{{ $employees['position']->department['department'] }}</td>

                            {{-- <td>{{ $employees['description']}}</td> --}}

                            <td>
                                {{$employees['email'] }}
                                <br>
                                {{$employees['cell_phone_number']}}
                            </td>
                            <td>
                                <form action="{{ route('employee.destroy',$employees->id) }}" method="POST">
                                    <a href=" {{route('employee.show',$employees['id'])}}" class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="view"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('employee.edit',$employees['id']) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>

                                    @csrf
                                    @method('DELETE')
                                 <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></button>
                                </form>
                            </td>

                          </tr>
                            @endforeach
                        </tbody>
                    </table>
                      </div>
                        <div class="d-flex justify-content-end">
                             {!! $employee->links() !!}
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </section>
        </div>
        <script>
            var input = document.getElementById("myInput");
            input.addEventListener("keyup", function(event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    document.getElementById("myBtn").click();
                }
            });
        </script>
    </x-base-layout>
