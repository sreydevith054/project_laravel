<x-base-layout>
    <div>
        @section('title','all list')
        <section class="section">
            <h1 class="section-header">
              <div>All List</div>
            </h1>
            <div class="section-body">
                <div class="row ">
                    <div class="col-2">
                        <div class="form-group">
                            <select class="form-control" name="department_id">
                                <option value="default">Choose</option>
                                <option>Department</option>
                                <option>Position</option>
                            </select>
                        </div>
                    </div>
                </div>
              <div class="row mt-1">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <div class="float-right">
                        <form>
                          <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-secondary"><i class="ion ion-search"></i></button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <h4>List</h4>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <tbody><tr>
                            {{-- <th class="text-center">
                              <div class="custom-checkbox custom-control">
                                <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                                <label for="checkbox-all" class="custom-control-label"></label>
                              </div>
                            </th> --}}
                            <th>ID</th>
                            <th>Department</th>
                            <th>Position</th>
                            <th>Employee</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                          @foreach ($department->all() as $departments)
                          <tr>
                            {{-- <td width="40">
                              <div class="custom-checkbox custom-control">
                                <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-1">
                                <label for="checkbox-1" class="custom-control-label"></label>
                              </div>
                            </td> --}}
                            <td>{{ $departments['id'] }}</td>
                            <td>
                                {{-- <img alt="image" src="{{ asset('image/'.$departments['image_path']) }}" class="rounded" width="100" data-toggle="title" title="Wildan Ahdian"> --}}
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>2018-01-20</td>
                            <td>
                                <div class="badge badge-success">Allow</div>
                            </td>
                            <td><a href="#" class="btn btn-action btn-secondary">Detail</a></td>
                          </tr>
                          @endforeach
                        </tbody>
                    </table>
                    {{-- @foreach ($position as $item)
                    <p>{{ $item->position}}</p>
                    @endforeach --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
    </x-base-layout>
