<x-base-layout>
<div>
    @section('title','All Department')

    <section class="section">
        @if (session()->has('message'))
            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                role="alert">
                <div class="flex">
                    <div>
                        <p class="text-sm">{{ session('message') }}</p>
                    </div>
                </div>
            </div>
        @endif
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div>All Department</div>
                {{-- @else --}}
                {{-- <div>Setting Department</div> --}}
                {{-- @endif --}}
        </h1>
        {{-- @if($isModalOpen) --}}
        {{-- @endif --}}
        <div class="section-body">
          <div class="row mt-5">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  <div class="float-right">
                      {{-- SEARCH FUNCTION --}}
                    <form action="">
                      <div class="input-group">
                        <input type="text" id="myInput" name="search" value="{{ $search }}" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                          <button type="submit" id="myBtn" class="btn btn-secondary"><i class="ion ion-search"></i></button>
                        </div>
                      </div>
                    </form>
                    {{-- <form>
                        <input id="myInput" name="q" value="{{ $search }}" placeholder="Some text..">
                        <input type="submit" id="myBtn" value="Submit">
                    </form> --}}

                  </div>
                  <h4>Department List</h4>
                  <a href="{{ route('department.create') }}" class="btn btn-sm btn-primary ion-plus-circled"></a>

                </div>

                <div class="card-body">
                    {{-- <a href="#" wire:click="create()" type="button" class="btn btn-info btn-sm mb-2">Add New</a> --}}

                  <div class="table-responsive">
                    <div class="d-flex justify-content-end">
                    </div>
                    <table class="table table-striped">
                      <tbody>
                          <tr>
                        {{-- <th>ID</th> --}}
                        <th>Picture</th>
                        <th>Department</th>
                        <th>Members</th>
                        <th>Position</th>
                        {{-- <th>Contact</th> --}}
                        <th>Action</th>
                      </tr>
                      @foreach($department as $departments)
                      <tr>
                        {{-- <td>{{ $departments['id'] }}</td> --}}
                        <td>
                            <img alt="image" src="{{ asset('image/'.$departments['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="Wildan Ahdian">
                        </td>

                        <td>
                            {{ $departments['department'] }}
                        </td>
                        <td>
                            @forelse ($departments->employee as $item )
                            <img alt="image" src="{{ asset('Employee/'.$item['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="{{ $item->name }}">
                            @empty
                            none
                            @endforelse
                            </div>
                        </td>
                        <td>
                            <div class="badge badge-info">
                                {{ $departments->position->count() }}
                            </div>

                        </td>
                        {{-- <td>{{ $departments['phone_num'] }}</td> --}}
                        <td>
                            <form action="{{ route('department.destroy',$departments->id) }}" method="POST">
                                <a href=" {{ url('admin/department/'.$departments->id.'/home') }}" class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="view"><i class="fas fa-eye"></i></a>
                                <a href="{{ route('department.edit',$departments['id']) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>
                                {{-- <a href="{{ route('department.show',$departments['id']) }}" class="btn btn-success btn-action" data-toggle="tooltip" title="view"><i class="fas fa-eye"></i></a> --}}

                          @csrf
                          @method('DELETE')
                        {{-- <button type="submit" class="btn btn-danger">Delete</button> --}}
                <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></button>
                        </form>

                            {{-- <a class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="view"><i class="fas fa-eye"></i></a>
                            <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>
                            <a class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></a> --}}
                        </td>
                      </tr>
                        @endforeach
                    </tbody>
                </table>
                  </div>
                    <div class="d-flex justify-content-end">
                         {!! $department->links() !!}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <script>
        var input = document.getElementById("myInput");
        input.addEventListener("keyup", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                document.getElementById("myBtn").click();
            }
        });
    </script>
</x-base-layout>
