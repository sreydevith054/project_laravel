<div class="row">
    <div class="col-md-6">
        {{-- header --}}
        <div class="col-xs-12 col-sm-12 col-md-12 d-none">
            <div class="form-group">
                <strong>Position:</strong>
                <select class="form-control" name="position_id">
                <option value="{{ $position['id'] }}">{{ $position['department']->department }} | {{ $position['position']}}</option>
                </select>
            </div>
        </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Gender:</strong>
                <select class="form-control" name="gender">
                <option value="default">Choose</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>

                </select>
            </div>
        </div>
        </div>
    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Date Of Birth:</strong>
                <input id="date" type="date" name="dob" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Educational institution</strong>
                <input type="text" name="learning_institute" class="form-control" placeholder="Educational Institution">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <div class="form-group">
                    <strong>Marital Status</strong>
                    <select class="form-control" name="marital_status">
                    <option value="default">Choose</option>
                    <option value="Single">Single</option>
                    <option value="Married">Married</option>
                    <option value="Windowed">Windowed</option>
                    <option value="Divorced">Divorced</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Spouse's Name:</strong>
                <input type="text" name="Spouses_name" class="form-control" placeholder="Spouse's Name (Optional)">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Spouse's Contact:</strong>
                <input type="text" name="Spouses_contact" class="form-control" placeholder="Spouse's Contact (Optional)">
            </div>
        </div>
        </div>
            </div>


        <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
            <div class="form-group">
                <strong>Address</strong>
                <textarea class="form-control" rows="3" name="address" placeholder="Address"></textarea>
            </div>
        </div>
        {{-- <div class="col-xs-12 col-sm-12 col-md-12 my-3">
            @include('livewire.admin.include.button')
        </div> --}}

    </div>

    <div class="col-md-6">
    {{-- <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
            <strong>Image:</strong>
            <input type="file" name="image" class="form-control" placeholder="employee">
        </div>
    </div> --}}
    <div class="row d-flex justify-content-center">
        <div class="col-xs-9 col-sm-9 col-md-9 mb-3">
            <strong>Image:</strong>
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="chooseFile">
                <label class="custom-file-label" for="chooseFile">Select file</label>
            </div>
            </div>
    </div>


    <div class="user-image mb-3 text-center">
        <div style="width: 300px; height: 300px; overflow: hidden; background: #cccccc; margin: 0 auto">
            <img src="..." class="figure-img img-fluid rounded"  id="imgPlaceholder" alt="">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row d-flex justify-content-center ">

            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <strong>Description:</strong>
                    <textarea class="form-control" rows="1" name="description" placeholder="Description"></textarea>
                </div>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type='email' name="email" class="form-control" placeholder="Email">
                </div>
            </div>

            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>Phone Number:</strong>
                    <input type="text" name="phone_number" class="form-control" placeholder="Phone Number">
                </div>
            </div>

            <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                        <strong>Cell Phone:</strong>
                        <input type="text" name="cell_phone_number" class="form-control" placeholder="Cell Phone">
                    </div>
            </div>
        </div>

    </div>
</div>


