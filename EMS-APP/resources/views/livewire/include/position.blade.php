  <div class="card-body">
    <a class="btn btn-primary ion-plus-circled btn-sm mb-2" href="{{ route('department.position.create',$department->id) }}"></a>
    <div class="table-responsive">

      <table class="table table-striped">
        <tbody>
        <tr>
          <th>ID</th>
          <th>Position</th>
          <th>Members</th>
          <th>Description</th>
          <th>Created Date</th>
          <th>Action</th>
        </tr>
        @foreach($position as $positions)
        <tr>
            <td>
                {{ $positions['id'] }}
            </td>
          <td>
                {{ $positions['position'] }}
          </td>
          <td>
            @forelse ($positions->employee as $item)
            <img alt="image" src="{{ URL::to('Employee/'.$item['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="{{ $item->name }}">
            @empty
            <strong>None</strong>
            @endforelse
          </td>
          <td>{{ $positions->description }}</td>
          <td>
                {{
                    date_format($positions->created_at,"d-m-Y")
                }}
            </td>
          <td>
            <form action="{{ route('department.position.destroy',[$department->id,$positions->id]) }}" method="POST">
                @csrf
                @method('DELETE')
              <a href=" {{route('department.position.show',[$department->id,$positions->id])}}" class="btn btn-action btn-info">Detail</a>
              <a href="{{ route('department.position.edit',[$department->id,$positions->id]) }}" class="btn btn-dark btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>
              <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></button>
            </form>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>

    </div>
    <div class="row d-flex justify-content-center">
        {!!$position->render() !!}
      </div>

  </div>
