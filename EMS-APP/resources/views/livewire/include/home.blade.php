<div class="row mt-2">
    {{-- position counter --}}
    <div class="col-lg-6">
      {{-- <div class="col-12 col-sm-6 col-lg-3"> --}}
          <div class="card card-sm bg-primary">
            <div class="card-icon">
              <i class="far fa-building"></i>
            </div>
            <div class="card-wrap">
              <div class="card-body">
                {{ $department['position']->count() }}
              </div>
              <div class="card-header">
                <h4>Position</h4>
              </div>
            </div>
          </div>
        {{-- </div> --}}
        {{-- //employee counter --}}
        {{-- <div class="col-12 col-sm-6 col-lg-3"> --}}
          <div class="card card-sm bg-success">
            <div class="card-icon">
              <i class="fas fa-users"></i>
            </div>
            <div class="card-wrap">
              <div class="card-body">
                {{$department['employee']->count()}}
              </div>
              <div class="card-header">
                <h4>Employee</h4>
              </div>
            </div>
          </div>
          <div class="card card-sm bg-info">
              <div class="card-icon">
                <i class="ion-ios-telephone"></i>
              </div>
              <div class="card-wrap">
                <div class="card-body">
                    {{ $department['phone_num'] }}
                </div>
                <div class="card-header">
                  <h4>Contact</h4>
                </div>
              </div>
            </div>

        {{-- </div> --}}




      </div>
      <div class="col-md-6">
      <div class="user-image mb-3 text-center">
          {{-- <div style="width: 400px; height: 400px; overflow: hidden; background: #cccccc; margin: 0 auto"> --}}
              <img src="{{ URL::to('image/',$department['image_path'])}}" width="500px" height="500px" class="figure-img img-fluid rounded" id="imgPlaceholder" alt="">
          {{-- </div> --}}

        </div>
      </div>
    </div>

