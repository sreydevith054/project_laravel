<x-base-layout>
    @section('title','Add New Employee')
            <section class="section">
                    <h1 class="section-header">
                        {{-- @if ($updateMode) --}}
                        <div>Add New Employee</div>
                        {{-- @else --}}
                        {{-- <div>Setting employee</div> --}}
                        {{-- @endif --}}
                </h1>

                <div class="row">
                    <div class="col-lg-5 margin-tb">
                    </div>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <form action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @include('livewire.admin.include.create-employee')

                    </form>
                </div>
            </section>
    @push('script')
    <!-- jQuery -->
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
    <script>
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            $('#imgPlaceholder').attr('src', e.target.result);
            }

            // base64 string conversion
            reader.readAsDataURL(input.files[0]);
        }
        }

        $("#chooseFile").change(function () {
        readURL(this);
        });

    $("input").on("change", function() {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
            )
        }).trigger("change")
    </script>

    @endpush
</x-base-layout>
