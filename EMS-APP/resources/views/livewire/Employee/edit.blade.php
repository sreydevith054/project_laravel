<x-base-layout>
    @section('title','Add New Employee')
    <section class="section">
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div>Update Employee Info</div>
                {{-- @else --}}
                {{-- <div>Setting employee</div> --}}
                {{-- @endif --}}
        </h1>

<div class="row">
    <div class="col-lg-5 margin-tb">
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('employee.update',$employee['id']) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
     <div class="row">
         <div class="col-md-6">
            {{-- header --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Position:</strong>
                    <select class="form-control" name="position_id">
                    <option class="d-none" value="{{ $employee->position_id }}">{{ $employee->position->department->department }} | {{ $employee->position->position }}</option>
                    @foreach ($position as $positions)
                    <option value="{{ $positions['id'] }}">{{ $positions['department']->department }} | {{ $positions['position']}}</option>
                    @endforeach
                    </select>
                </div>
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" value="{{ $employee['name'] }}" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Gender:</strong>
                    <select class="form-control" name="gender">
                    <option class="d-none" value="{{ $employee['gender'] }}">{{ $employee['gender'] }}</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>

                    </select>
                </div>
            </div>
            </div>
        </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Date Of Birth:</strong>
                    <input id="date" type="date" data-date="" data-date-format="DD/MM/YYYY" value="{{ $employee['dob'] }}" name="dob" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Educational institution</strong>
                    <input type="text" value="{{ $employee['learning_institute'] }}" name="learning_institute" class="form-control" placeholder="Educational Institution">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="form-group">
                        <strong>Marital Status</strong>
                        <select class="form-control" name="marital_status">
                        <option class="d-none" value="{{ $employee['marital_status'] }}">{{ $employee['marital_status'] }}</option>
                        <option value="Single">Single</option>
                        <option value="Married">Married</option>
                        <option value="Windowed">Windowed</option>
                        <option value="Divorced">Divorced</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                      <strong>Spouse's Name:</strong>
                      <input type="text" value="{{ $employee['Spouses_name'] }}" name="Spouses_name" class="form-control" placeholder="Spouse's Name (Optional)">
                  </div>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                      <strong>Spouse's Contact:</strong>
                      <input type="text" value="{{ $employee['Spouses_contact'] }}" name="Spouses_contact" class="form-control" placeholder="Spouse's Contact (Optional)">
                  </div>
              </div>
              </div>
                </div>


            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                <div class="form-group">
                    <strong>Address</strong>
                    <textarea class="form-control" rows="3" name="address" placeholder="Address">{{ $employee['address'] }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 my-3">
                <a href="{{ route('employee.index') }}" class="btn btn-danger  btn-sm" role="button" data-toggle="tooltip" title="" data-original-title="Back"><i class="ion-arrow-left-a"></i></a>
            </div>

         </div>

         <div class="col-md-6">
        {{-- <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Image:</strong>
                <input type="file" name="image" class="form-control" placeholder="employee">
            </div>
        </div> --}}
        <div class="row d-flex justify-content-center">
            <div class="col-xs-9 col-sm-9 col-md-9 mb-3">
                <strong>Image:</strong>
                <div class="custom-file">
                    <input type="file"  name="image" value="{{ $employee['image_path']}}" class="custom-file-input" id="chooseFile">
                    <label class="custom-file-label" for="chooseFile">{{ $employee['image_path']}}</label>
                </div>
                <div class="user-image mt-3 text-center">
                    <div style="width: 300px; height: 300px; overflow: hidden; background: #cccccc; margin: 0 auto">
                        <img src="{{ URL::to('Employee/'.$employee['image_path']) }}" class="figure-img img-fluid rounded"  id="imgPlaceholder" alt="">
                    </div>
                  </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row d-flex justify-content-center ">

                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="form-group">
                        <strong>Description:</strong>
                        <textarea class="form-control" rows="1" name="description" placeholder="description">{{ $employee['description'] }}</textarea>
                    </div>
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input type='email' value="{{ $employee['email'] }}" name="email" class="form-control" placeholder="Email">
                    </div>
                </div>

                <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                        <strong>Phone Number:</strong>
                        <input type="text" value="{{ $employee['phone_number'] }}" name="phone_number" class="form-control" placeholder="Phone Number">
                    </div>
                </div>

                <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Cell Phone:</strong>
                            <input type="text" value="{{  $employee['cell_phone_number'] }}" name="cell_phone_number" class="form-control" placeholder="Cell Phone">
                        </div>
                </div>
            </div>
            <div class="form-group">
                <button type="Submit" class="btn btn-primary btn-sm float-right mr-5" role="button" data-toggle="tooltip" title="" data-original-title="update"><i class="ion-checkmark-circled"></i></button>
            </div>

        </div>
    </div>

</form>
</div>
</section>
@push('script')
<!-- jQuery -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPlaceholder').attr('src', e.target.result);
        }

        // base64 string conversion
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#chooseFile").change(function () {
      readURL(this);
    });

$("input").on("change", function() {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format( this.getAttribute("data-date-format") )
        )
    }).trigger("change")
        </script>

@endpush
</x-base-layout>
