<link rel="stylesheet" href="{{ URL::to('../dist/modules/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/modules/ionicons/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">

<link rel="stylesheet" href="{{ URL::to('../dist/modules/summernote/summernote-lite.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/modules/flag-icon-css/css/flag-icon.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/css/demo.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/css/style.css') }}">
