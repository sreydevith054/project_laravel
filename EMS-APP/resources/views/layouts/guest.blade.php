<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials._head')
    @include('partials.style')
    @livewireStyles
</head>
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>

    {{-- <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div> --}}
            {{-- navbar --}}
          {{-- @include('include.navbar') --}}

          {{-- end-navbar --}}
          {{-- main-sidebar --}}
          {{-- @include('include.main-sidebar') --}}

          @include('include.navbar')

          {{-- main-content --}}
            {{ $slot }}
          {{-- end-main-content --}}
        </div>
    </div>

          {{-- <footer class="main-footer">
            <div class="footer-left">
              Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://multinity.com/">Multinity</a>
            </div>
            <div class="footer-right"></div>
          </footer> --}}
        </div>
      </div>


    {{-- @yield('content') --}}

@include('partials._script')

<script type="text/javascript">

    var url = "{{ route('changelang') }}";

    $(".changeLang").change(function(){
        window.location.href = url + "?lang="+ $(this).val();
    });

</script>

@livewireScripts
</body>
</html>
