<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials._head')
    @include('partials.style')
    @stack('css')
    @livewireStyles
</head>
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            {{-- navbar --}}

          @include('include.navbar')

          {{-- end-navbar --}}
          {{-- main-sidebar --}}
          @if (Route::has('login'))
            @auth
            @if (Auth::user()->user_type==='admin'||Auth::user()->user_type==='user')
                @if (Route('dashboard'))
                @include('include.main-sidebar')
                @endif
            @endif
            @endauth
          @endif
        <div class='main-content'>
          {{-- main-content --}}
            {{ $slot }}
          {{-- end-main-content --}}
        {{-- </div> --}}
          {{-- <footer class="main-footer">
            <div class="footer-left">
              Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://multinity.com/">Multinity</a>
            </div>
            <div class="footer-right"></div>
          </footer> --}}
        {{-- </div>
      </div> --}}

    {{-- @yield('content') --}}

@include('partials._script')
@livewireScripts

@stack('script')
<script type="text/javascript">

    var url = "{{ route('changelang') }}";

    $(".changeLang").change(function(){
        window.location.href = url + "?lang="+ $(this).val();
    });

</script>
</body>
</html>
