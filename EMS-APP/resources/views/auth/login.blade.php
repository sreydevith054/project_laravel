{{-- <x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="auth" value="{{ __('Email/Username/Phone Number') }}" />
                <x-jet-input id="auth" class="block mt-1 w-full" type="text" name="auth" :value="old('auth')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Log in') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout> --}}
<x-guest-layout>
{{-- @extends('layouts.guest') --}}
@section('title','Login')
@section('content')
<body class="sidebar-gone">
    <div id="app">
      <section class="section">
        <div class="container mt-5">
          <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4 mt-5">
              <div class="login-brand mt-5">
                {{-- EMS System --}}
              </div>
            <x-jet-validation-errors class="mb-4" />

              @if (session('status'))
                  <div class="mb-4 font-medium text-sm text-green-600">
                      {{ session('status') }}
                  </div>
              @endif

              <div class="card bg-dark card-primary">
                <div class="card-header"><h4>Login</h4></div>

                <div class="card-body">

                  <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                       @csrf

                    <div class="form-group">
                      <label for="auth">Email/Username/Phone Number</label>
                      <input id="auth" type="text" class="form-control" name="auth" :value="old('auth')" tabindex="1" required="" autofocus="">
                      <div class="invalid-feedback">
                        Please fill in your Email Username or PhoneNumber
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="password" class="d-block">Password
                        <div class="float-right">
                     @if (Route::has('password.request'))
                          <a href="{{ route('password.request') }}">
                            Forgot Password?
                          </a>
                    @endif
                        </div>
                      </label>
                      <input id="password" type="password" class="form-control" name="password" tabindex="2" required=""
                      autocomplete="current-password"
                      >
                      <div class="invalid-feedback">
                        please fill in your password
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember_me">
                        <label class="custom-control-label" for="remember_me">Remember Me</label>
                      </div>
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                        Login
                      </button>
                    </div>
                  </form>
                </div>
              </div>
              {{-- <div class="mt-5 text-muted text-center">
                Don't have an account? <a href="{{ route('register') }}">Create One</a>
              </div>
              <div class="simple-footer">
                Copyright © EMS 2021
              </div> --}}
            </div>
          </div>
        </div>
      </section>
    </div>
  </body>
</x-guest-layout>
