    <section class="section">
      <h1 class="section-header">
        <div>{{ __('massages.Dashboard') }}</div>
      </h1>
      <div class="row d-flex justify-content-center">
        <div class="col-lg-4 col-md-6 col-12">
          <div class="card card-sm-3">
            <div class="card-icon bg-primary">
              <i class="ion ion-person"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>{{ __('massages.TOTAL USERS') }}</h4>
              </div>
              <div class="card-body">
                {{$users}}
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 col-12">
          <div class="card card-sm-3">
            <div class="card-icon bg-danger">
                <i class="fas fa-users"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>{{ __('massages.EMPLOYEE') }}</h4>
              </div>
              <div class="card-body">
                {{ $employee }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-12">
          <div class="card card-sm-3">
            <div class="card-icon bg-success">
                <i class="far fa-building"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>{{ __('massages.DEPARTMENT') }}</h4>
              </div>
              <div class="card-body">
                {{ $department }}
              </div>
            </div>
          </div>
        </div>
        {{-- <div class="col-lg-3 col-md-6 col-12">
            <div class="card card-sm-3">
              <div class="card-icon bg-warning">
                <i class="ion ion-paper-airplane"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Message</h4>
                </div>
                <div class="card-body">
                  0
                </div>
              </div>
            </div>
          </div> --}}

      </div>

      {{-- <div class="row">
        <div class="col-lg-8 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <div class="float-right">
                <div class="btn-group">
                  <a href="#" class="btn active">Week</a>
                  <a href="#" class="btn">Month</a>
                  <a href="#" class="btn">Year</a>
                </div>
              </div>
              <h4>Statistics</h4>
            </div>
            <div class="card-body">
              <canvas id="myChart" height="158"></canvas>
              <div class="statistic-details mt-sm-4">
                <div class="statistic-details-item">
                  <small class="text-muted"><span class="text-primary"><i class="ion-arrow-up-b"></i></span> 7%</small>
                  <div class="detail-value">$243</div>
                  <div class="detail-name">Today's Sales</div>
                </div>
                <div class="statistic-details-item">
                  <small class="text-muted"><span class="text-danger"><i class="ion-arrow-down-b"></i></span> 23%</small>
                  <div class="detail-value">$2,902</div>
                  <div class="detail-name">This Week's Sales</div>
                </div>
                <div class="statistic-details-item">
                  <small class="text-muted"><span class="text-primary"><i class="ion-arrow-up-b"></i></span>9%</small>
                  <div class="detail-value">$12,821</div>
                  <div class="detail-name">This Month's Sales</div>
                </div>
                <div class="statistic-details-item">
                  <small class="text-muted"><span class="text-primary"><i class="ion-arrow-up-b"></i></span> 19%</small>
                  <div class="detail-value">$92,142</div>
                  <div class="detail-name">This Year's Sales</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h4>Recent Activities</h4>
            </div>
            <div class="card-body">
              <ul class="list-unstyled list-unstyled-border">
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="../dist/img/avatar/avatar-1.jpeg" alt="avatar">
                  <div class="media-body">
                    <div class="float-right"><small>10m</small></div>
                    <div class="media-title">Farhan A Mujib</div>
                    <small>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</small>
                  </div>
                </li>
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="../dist/img/avatar/avatar-2.jpeg" alt="avatar">
                  <div class="media-body">
                    <div class="float-right"><small>10m</small></div>
                    <div class="media-title">Ujang Maman</div>
                    <small>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</small>
                  </div>
                </li>
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="../dist/img/avatar/avatar-3.jpeg" alt="avatar">
                  <div class="media-body">
                    <div class="float-right"><small>10m</small></div>
                    <div class="media-title">Rizal Fakhri</div>
                    <small>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</small>
                  </div>
                </li>
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="../dist/img/avatar/avatar-4.jpeg" alt="avatar">
                  <div class="media-body">
                    <div class="float-right"><small>10m</small></div>
                    <div class="media-title">Alfa Zulkarnain</div>
                    <small>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</small>
                  </div>
                </li>
              </ul>
              <div class="text-center">
                <a href="#" class="btn btn-primary btn-round">
                  View All
                </a>
              </div>
            </div>
          </div>
        </div>
      </div> --}}
      <div class="row">
        {{-- <div class="col-lg-5 col-md-12 col-12 col-sm-12">
          <form method="post" class="needs-validation" novalidate="">
            <div class="card">
              <div class="card-header">
                <h4>Quick Draft</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" name="title" class="form-control" required>
                  <div class="invalid-feedback">
                    Please fill in the title
                  </div>
                </div>
                <div class="form-group">
                  <label>Content</label>
                  <textarea class="summernote-simple"></textarea>
                </div>
              </div>
              <div class="card-footer">
                <button class="btn btn-primary">Save Draft</button>
              </div>
            </div>
          </form>
        </div> --}}
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <div class="float-right">
                <nav class="d-inline-block">
                    {{-- <ul class="pagination mb-0">
                      <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1"><i class="ion ion-chevron-left"></i></a>
                      </li>
                      <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                      <li class="page-item">
                        <a class="page-link" href="#">2</a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item">
                        <a class="page-link" href="#"><i class="ion ion-chevron-right"></i></a>
                      </li>
                    </ul> --}}
                    {{ $panel->links() }}

                  </nav>
                 </div>
              <h4>{{ __('massages.USERS PANNEL') }}</h4>
            </div>
            <div class="card-body text-center">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>N.O</th>
                      <th>Name</th>
                      <th>Role</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($panel as $panels)
                      <tr>
                        <td>
                          {{ $panels['id'] }}
                        </td>
                        <td>
                          <a href="{{ url('admin/profile') }}"><img src="{{ asset('storage/'.Auth::user()->profile_photo_path) }}" alt="{{ Auth::user()->name }}" width="50" class="rounded-circle img-thumbnail mr-1">{{ $panels['name'] }}</a>
                        </td>
                        <td>
                          {{ $panels['user_type'] }}
                        </td>

                        <td>
                          <a class="btn btn-success btn-action" data-toggle="tooltip" title="view"><i class="fas fa-eye"></i></a>
                          <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="ion ion-edit"></i></a>
                          <a class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="ion ion-trash-b"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    {{-- <tr>
                      <td>
                        IT-01
                      </td>
                      <td>
                        <a href="#"><img src="../dist/img/avatar/avatar-1.jpeg" alt="avatar" width="30" class="rounded-circle mr-1"> Bagus Dwi Cahya</a>
                      </td>
                      <td>
                        Admin
                      </td>

                      <td>
                        <a class="btn btn-success btn-action" data-toggle="tooltip" title="view"><i class="fas fa-eye"></i></a>
                        <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="ion ion-edit"></i></a>
                        <a class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="ion ion-trash-b"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        IT-02
                      </td>
                      <td>
                        <a href="#"><img src="../dist/img/avatar/avatar-1.jpeg" alt="avatar" width="30" class="rounded-circle mr-1"> Bagus Dwi Cahya</a>
                      </td>
                      <td>
                        Admin
                      </td>

                      <td>
                        <a class="btn btn-success btn-action" data-toggle="tooltip" title="view"><i class="fas fa-eye"></i></a>
                        <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="ion ion-edit"></i></a>
                        <a class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="ion ion-trash-b"></i></a>
                      </td>
                    </tr> --}}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
