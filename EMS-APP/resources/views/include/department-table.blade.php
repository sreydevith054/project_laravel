<div class="section-body">
    <div class="row mt-5">
      <div class="col-12">
        <div class="card">

          <div class="card-header">
            <div class="float-right">
              <form>
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search">
                  <div class="input-group-btn">
                    <button class="btn btn-secondary"><i class="ion ion-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
            <h4>Department List</h4>
          </div>
          <div class="card-body">
              {{-- <a href="#" wire:click="create()" type="button" class="btn btn-info btn-sm mb-2">Add New</a> --}}

            <div class="table-responsive">
              <div class="d-flex justify-content-end">
                  {{-- {!! $department->links() !!} --}}
              </div>
              <table class="table table-striped">
                <tbody>
                    <tr>
                  <th class="text-center">
                    <div class="custom-checkbox custom-control">
                      <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                      <label for="checkbox-all" class="custom-control-label"></label>
                    </div>
                  </th>
                  <th>ID</th>
                  <th>Picture</th>
                  <th>Department</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Contact</th>
                  <th>Action</th>
                </tr>
                @foreach($departments as $department)
                <tr>
                  <td width="40">
                    <div class="custom-checkbox custom-control">
                      <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-1">
                      <label for="checkbox-1" class="custom-control-label"></label>
                    </div>
                  </td>
                  <td>{{ $department['id'] }}</td>
                  <td>
                      <img alt="image" src="{{ asset('image/'.$department['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="Wildan Ahdian">
                    </td>

                  <td>
                      {{ $department['department'] }}
                  </td>
                  <td>{{ $department['description'] }}</td>
                  <td>
                      <div class="badge badge-success">Allow</div>
                  </td>
                  <td>{{ $department['phone_num'] }}</td>
                  <td>
                      <form action="{{ route('department.destroy',$department->id) }}" method="POST">
                          <a href="{{ route('department.edit',$department['id']) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>
                          {{-- <a href="{{ route('department.show',$departments['id']) }}" class="btn btn-success btn-action" data-toggle="tooltip" title="view"><i class="fas fa-eye"></i></a> --}}

                    @csrf
                    @method('DELETE')
                  {{-- <button type="submit" class="btn btn-danger">Delete</button> --}}
          <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></button>
                  </form>

                      {{-- <a class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="view"><i class="fas fa-eye"></i></a>
                      <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>
                      <a class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></a> --}}
                  </td>
                </tr>
                  @endforeach
              </tbody>
          </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
