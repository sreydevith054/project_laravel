<?php

return [
    'home'=>'home',
    'Dashboard' => 'Dashboard',
    'Users'=>'Users',
    'Department'=>'Department',
    'Position'=>'Position',
    'Employee'=>'Employee',
    'TOTAL USERS'=>'TOTAL USERS',
    'EMPLOYEE'=>'EMPLOYEE',
    'DEPARTMENT'=>'DEPARTMENT',
    'EMS'=>'EMS',
    'Login'=>'Login',
    'Register'=>'Register',
    'USERS PANNEL'=>'USERS PANNEL'
];
