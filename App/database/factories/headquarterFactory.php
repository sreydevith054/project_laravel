<?php

namespace Database\Factories;

use App\Models\Car_Models;
use App\Models\headquarter;
use Illuminate\Database\Eloquent\Factories\Factory;

class headquarterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = headquarter::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'car_id'=>Car_Models::factory(),
            'headquarter'=>$this->faker->city(),
            'country'=>$this->faker->country()
        ];
    }
}
