<?php

namespace Database\Factories;

use App\Models\Car_Models;
use App\Models\car_production_date;
use Illuminate\Database\Eloquent\Factories\Factory;

class car_production_dateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = car_production_date::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'model_id'=>Car_Models::factory(),
        //'created_at'=>$this->faker->date()
        ];
    }
}
