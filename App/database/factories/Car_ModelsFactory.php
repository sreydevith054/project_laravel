<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\Car_Models;
use Illuminate\Database\Eloquent\Factories\Factory;

class Car_ModelsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car_Models::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'car_id'=>Car::factory(),
        'Model_name'=>$this->faker->name()
        ];
    }
}
