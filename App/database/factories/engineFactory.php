<?php

namespace Database\Factories;

use App\Models\Car_Models;
use App\Models\engine;
use Illuminate\Database\Eloquent\Factories\Factory;

class engineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = engine::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'model_id'=>Car_Models::factory(),
            'engine_name'=>$this->faker->name(),
        ];
    }
}
