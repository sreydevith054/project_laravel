@extends('layouts.app')
@section('content')
 {{-- <div class="container py-5 ">

    <div class="col-md-5 m-auto">
        <div class="card">
    <div class="card-body">
        <h5 class="card-title">TYPE OF CARS:<span class="text-primary">{{$car['name']}}</span></h5>
        <p class="card-text">Headquarter:
            {{-- <span class="text-danger">{{$hp['headquarter']}}</span>
            <span class="text-info">{{$hp->country}}</span> --}}
        {{-- </p>

        <p class="card-text">Founded:<span class="text-danger">{{$car['founded']}}</span></p>
        <p class="card-text"><small class="text-primary">Description:<span class="text-success">{{$car['description']}}</span></small></p>
        @forelse ($car->Car_Models as $cars)
        <p class="card-text"><small class="text-info">Model:<span class="text-success">{{$cars['Model_name']}}</span></small></p>
        @empty
        @endforelse
    </div>
    <img src="https://images.hgmsites.net/hug/yamaha-sports-ride-concept-2015-tokyo-motor-show_100532098_h.jpg"
        class="card-img-bottom" alt="car">
    </div>
        <a name="back" id="back" class="btn btn-dark mt-2" href="{{route('cars.index')}}" role="button">Back</a>
    </div>
</div>
</div> --}}
    <h3 class="display-3 text-center">{{$car['name']}}</h3>
<div class="table-container pt-5 text-center">
    <div class="col-6 m-auto">
    <table class="table table-striped table-hover table-bordered">
    <thead>
        <tr class="table-dark">
            <th>Model</th>
            <th>Engine</th>
            <th>Date</th>
        <tr>
    </thead>
    <tbody>

        @forelse($car->Car_Models as $models)
        <tr>
            <td>
            {{$models['Model_name']}}
            </td>
            <td>
        @foreach ($car->Engine as $engine)

            @if ($models['id']==$engine['model_id'])
                {{$engine['Engine_Name'].","}}
            @endif

        @endforeach
            </td>
            <td>
            {{date('d-m-Y',strtotime($car->dateproduct->created_at))}}
            </td>
        </tr>
        @empty
        <p>Not Founded</p>
        @endforelse
    </tbody>
    </table>
</div>
</div>

<br>


@endsection
