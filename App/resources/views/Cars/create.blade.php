@extends('layouts.app')
@section('content')
    <div class="form-body style">
        <div class="row">
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Create New Car</h3>
                        <p>Fill in the data below.</p>
                        <form class="requires-validation" action="{{route('cars.store')}}" method="post" novalidate>
                            @csrf
                            <div class="col-md-12">
                               <input class="form-control" type="text" name="name" placeholder="Car's Name" required>
                               <div class="valid-feedback">Car's Name field is valid!</div>
                               <div class="invalid-feedback">Car's Name field cannot be blank!</div>
                            </div>

                            <div class="col-md-12">
                                <input class="form-control" type="text" name="founded" placeholder="Founded" required>
                                 <div class="valid-feedback">Founded field is valid!</div>
                                 <div class="invalid-feedback">Founded cannot be blank!</div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3 mt-4">
                                <textarea class="form-control" name="description" placeholder="Description" id="exampleFormControlTextarea1" rows="3" required></textarea>
                                 <div class="invalid-feedback">Description field cannot be blank!</div>
                                 <div class="valid-feedback">Description field is valid!</div>
                                </div>
                            </div>
                            {{-- <div class="col-md-12">
                                <input class="form-control" type="file" name="img" required>
                                 <div class="valid-feedback">Upload field is valid!</div>
                                 <div class="invalid-feedback">Upload cannot be blank!</div>
                            </div> --}}
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
