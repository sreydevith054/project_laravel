@extends('layouts.app')
    @section('content')
 <div class="container-sm mt-2">
        <h1 class="text-center display-1">Car List</h1>
        <a name="record"
             id="record"
             class="btn btn-info my-3"
             href="{{route('cars.create')}}"
             role="button"
             >Create New Record
        </a>
        <table class="table table-striped table-responsive ">
            <thead class="thead-inverse|thead-default bg-dark text-light text-center">
                <tr>
                    <th>ID</th>
                    {{-- <td>Image</td> --}}
                    <th>Name</th>
                    <th>Founded</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($cars as $car)
                    <tr>
                        <td scope="row">{{$car->id}}</td>
                        {{-- <td><img src="/image/{{$car->img}}" width="150px" class="img-fluid" alt=""></td> --}}
                        <td>{{$car->name}}</td>
                        <td>{{$car->founded}}</td>
                        <td>
                        {{\Illuminate\Support\Str::limit($car->description, 20)}}
                        </td>
                        <td>
                            <form action="{{route('cars.destroy',$car->id)}}" class="" method="post">
                                <a name="view"
                                id="view"
                                class="btn btn-success"
                                href="{{route('cars.show',$car->id)}}"
                                role="button"
                                >View
                                </a>
                                <a name="Edit"
                                id="Edit"
                                class="btn btn-info"
                                href="{{route('cars.edit',$car->id)}}"
                                role="button"
                                >Edit
                                </a>
                                    @csrf
                                    @method('delete')
                                <button
                                type="submit"
                                class="btn btn-danger"
                                >Delete
                                </button>
                            </form>
                        </td>

                    </tr>

                    @endforeach

                </tbody>
        </table>

    </div>
    @endsection

