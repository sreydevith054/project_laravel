<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Car as ModelsCar;
use App\Models\engine;
use App\Models\headquarter;
use Illuminate\Support\Facades\Log;

class CAR extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars=\App\Models\Car::all();
        //dd($cars);
        return view('Cars.index',['cars'=>$cars]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/Cars/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $cars=new ModelsCar;
        // $cars->name=$request->input('name');
        // $cars->founded=$request->input('founded');
        // $cars->description=$request->input('description');
        // // $cars->save();
        $cars=ModelsCar::make([
                'name'=>$request->input('name'),
                'founded'=>$request->input('founded'),
                'description'=>$request->input('description'),
                //'img'=>$request->input('img')
        ]);
    //     if ($image = $request->file('img')) {
    //         $destinationPath = 'image/';
    //         $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
    //         $image->move($destinationPath, $profileImage);
    //         $cars['img'] = "$profileImage";
    //     }
    //     //ModelsCar::create($input);
    //    // return redirect()->route('products.index')
    //     $cars->save();//if using create()
    // $input = $request->all();

    //     if ($image = $request->file('img')) {
    //         $destinationPath = 'image/';
    //         $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
    //         $image->move($destinationPath, $profileImage);
    //         $input['img'] = "$profileImage";
    //     }
        $cars->save();
       // ModelsCar::create($input);
        return redirect()->route('cars.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car=ModelsCar::find($id);
        //$hp=headquarter::find($id);
        //$engine=engine::find($id);
        //printf($car->dateproduct->all());
        return view('Cars.show',['car'=>$car]);//'hp'=>$hp]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car=ModelsCar::find($id);
        //dd($car);
        return view('Cars.edit')->with('car',$car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $cars=ModelsCar::where('id',$id)
            ->update([
                'name'=>$request->input('name'),
                'founded'=>$request->input('founded'),
                'description'=>$request->input('description')
        ]);
        return redirect('/cars');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car=ModelsCar::find($id);
        $car->delete();
        return redirect()->route('cars.index');
    }
}
