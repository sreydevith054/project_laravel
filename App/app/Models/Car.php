<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $table="cars";
    protected $PrimaryKey="id";
    protected $fillable=[
        'name',
        'founded',
        'description',
        //'img'
    ];
    public function Car_Models()
    {
        return $this->hasMany(Car_Models::class);
    }

    public function headquarter()
    {
        return $this->hasOne(headquarter::class);
    }
    public function Engine()
    {
        return $this->hasManyThrough(
            engine::class,
            Car_Models::class,
            'car_id', //foriegn key on car_model
            'model_id'//foriegn key on engine
        );
    }

    public function dateproduct()
    {
        return $this->hasOneThrough(
            car_production_date::class,
            Car_Models::class,
            'car_id',
            'model_id'
        );
    }
}
// 'model_id',
//         'created_at'
