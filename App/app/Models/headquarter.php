<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class headquarter extends Model
{
    use HasFactory;
    protected $table="headquarters";
    protected $PrimaryKey="id";
    protected $fillable=[
        'car_id',
        'headquarter',
        'country'
        //'img'
    ];

    public function Car()
    {
        return $this->belongsTo(Car::class);
    }
}
