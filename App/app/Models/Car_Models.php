<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car_Models extends Model
{
    use HasFactory;
   protected $table="cars_models";
   protected $PrimaryKey="id";
   protected $fillable=[
        'car_id',
        'Model_name',
    ];
    public function Car()
    {
        return $this->belongsTo(Car::class);
    }
}
