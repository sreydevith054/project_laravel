<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class car_production_date extends Model
{
    use HasFactory;
    protected $fillable=[
        'model_id',
        'created_at'
    ];
}
